
public class Profesor extends Persona implements empleado  {
	
	 private int a�o_de_incorporacion;
	 private int N_de_anexo;
	 private String departamento;
	 
    Profesor(String nombre, String apellido, int n_de_identificacion, int a�o_de_incorporacion, int N_anexo, String departamento){
		 
		 super(nombre, apellido, n_de_identificacion);
		 this.a�o_de_incorporacion = a�o_de_incorporacion;
         this.N_de_anexo = N_anexo;
         this.departamento = departamento;
		
	 }
    
    
 
	 public int SetN_de_anexo(int N_de_anexo) {
		 
		 return N_de_anexo;
	 }
	 
    
	 public int SetA�o_de_incorporacion(int a�o_de_incorporacion) {
		
		 return a�o_de_incorporacion;
		 
	 }
	 
	 public String SetDepartamento(String departamento) {
		 
		 return departamento;
		 
	 }
	 
	 public void imprimir () {
		 
		    System.out.println("\nProfesor"); 
	 		System.out.println("Nombre: " + getNombre() + " Apellido: "+ getApellido());
	 		System.out.println("N� de indentificacion: "+ getN_de_identificacion());
	 		System.out.println("A�o de incorporacion: "+ this.a�o_de_incorporacion);
	 		System.out.println("N� de anexo : " + this.N_de_anexo );
	 		System.out.println("Departamento: "+ this.departamento);
	 }

}
