 class Persona {/* persona dentro de la universidad */
	 
	 private String nombre;
	 private String apellido;
	 private int N_de_identificacion;
	 
	 Persona(String nombre, String apellido, int N_de_identificacion){
		 
		 this.nombre = nombre;
		 this.apellido = apellido;
		 this.N_de_identificacion = N_de_identificacion;
		 
	 }
	 
	 public String getNombre() {
			
			return nombre;
		}

	
	public String getApellido() {
		
		return apellido;
	}
	
    
    public int getN_de_identificacion() {
		
		return N_de_identificacion;
	}
	
    
    public void imprimir() {
    	
    	System.out.println("Nombre: " + this.nombre + " Apellido: "+ this.apellido + "\nN de indentificacion: "+ this.N_de_identificacion);
    }
}
