import java.util.ArrayList;


public class Crear_Lista{
	

        private ArrayList <Estudiante> estudiantes = new ArrayList<Estudiante>() ;
		private ArrayList <Profesor>profesores = new ArrayList<Profesor>();
		private ArrayList<Administrativo>administrativos = new ArrayList<Administrativo>();
		
       
		public ArrayList<Estudiante> getEstudiantes(){
			
			return estudiantes;
		}
		public void setEstudiantes(Estudiante a, Estudiante b, Estudiante m) {
			 estudiantes.add(a);
	         estudiantes.add(b);
	         estudiantes.add(m);		
			
		}
		
		 
		public ArrayList<Profesor> getProfesores(){
			
			return profesores;
		}
		public void setProfesores(Profesor c, Profesor d, Profesor f) {
			 profesores.add(c);
	         profesores.add(d);
	         profesores.add(f);
			
		}
		
		 
		public ArrayList<Administrativo> getAdministrativo(){
			
			return administrativos;
		}
		public void setAdministrativos(Administrativo e, Administrativo f, Administrativo j) {
			 administrativos.add(e);
	         administrativos.add(f);
	         administrativos.add(j);
			
		}
		
		public void buscar(String Nombre) {
			
			for (Estudiante r : this.estudiantes) {
				if (Nombre == r.getNombre()) {
					r.imprimir();
				}	
			}
			for (Profesor r : this.profesores) {
				if (Nombre == r.getNombre()) {
					r.imprimir();
				}	
			}
			for (Administrativo r : this.administrativos) {
				if (Nombre == r.getNombre()) {
					r.imprimir();
				}	
			}
		}
}
