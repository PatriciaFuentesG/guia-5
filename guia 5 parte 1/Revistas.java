
public class Revistas implements Controlador {
	
	private int codigo;
	private String Titulo;
	private String prestar = "No prestado";
	private int a�o_de_publicacion;
	private boolean prestado;
	private boolean perdido;
	private int N_de_edicion;
	private String genero;
	
	Revistas(int codigo, String Titulo, int a�o_de_publicacion, int N_de_edicion, String genero ){
		
		this.codigo = codigo;
		this.Titulo = Titulo;
		this.a�o_de_publicacion = a�o_de_publicacion;
		this.prestado = false;
		this.perdido = false;
		this.N_de_edicion = N_de_edicion;
		this.genero = genero;
		
	}
	
	public String genero () {
		
		return genero;
	}
	public int GetN_de_edicion() {
		
		return N_de_edicion;
	}
	public int GetCodigo() {
		
		return codigo;	
	}
	
    public String GetTitulo() {
		
		return Titulo;	
	}
    
    public int GetA�o_de_publicacion() {
		
		return a�o_de_publicacion;	
	}

    public void Prestar() {
    	
    	this.prestado = true;
    	prestar = "Prestado";
    }
    
    public void Perdido() {
    	
    	System.out.println("A esta revista se le considera perdida");
    	this.perdido = true;
    }
    
    public int Tiempo_fuera() {
    	
    
    	if( genero == "comic") {
   	      int horas = ( int)(Math.random()*25+0);
	     return horas;
    	}
	    else {
	      int dias = ( int)(Math.random()*7+1);
	   
   	      return dias ;
	    }
    	
    	
    }
   
    
    public void imprimir_informacion() {
		
        System.out.println(Titulo +" , "+ a�o_de_publicacion + " ISBN: " + codigo + " - "+ prestar);
	}
   public void imprimir_a�oYcodigo() {
	     	 System.out.println( a�o_de_publicacion+ "ISBN" + codigo );
	 }

}
