

public class Libros implements Controlador{
	
	private int codigo;
	private String autor;
	private String Titulo;
	private String prestar = "No prestado";
	private int a�o_de_publicacion;
	private boolean prestado;
	private int dias;
	
	Libros(int codigo, String Titulo, int a�o_de_publicacion, String autor){
		
		this.codigo = codigo;
		this.Titulo = Titulo;
		this.a�o_de_publicacion = a�o_de_publicacion;
		this.prestado = false;
		this.autor = autor;
		
	}
	
	public int GetCodigo() {
		
		return codigo;	
	}
	
    public String GetTitulo() {
		
		return Titulo;	
	}
    
    public int GetA�o_de_publicacion() {
		
		return a�o_de_publicacion;	
	}

    public void Prestar() {
    	
    	this.prestado = true;
        prestar = "Prestado";
    }
    
  
    
    public int Tiempo_fuera() {
    	
	    dias = ( int)(Math.random()*7+1);
	 
    	
    	return dias ;
    	
    }
    
    public String autor() {
    	
    	return autor;
    }
    
    public void imprimir_informacion() {
		
           System.out.println(Titulo +" - "+ autor +" , "+ a�o_de_publicacion + " ISBN: " + codigo +" - "+ prestar);
	}
    public void imprimir_a�oYcodigo() {
	     	 System.out.println( a�o_de_publicacion+ "ISBN" + codigo );
	   }
	
}