import java.time.LocalDateTime;
import java.util.ArrayList;

public class Caracteristicas {
	
	private ArrayList <Revistas> revistas = new ArrayList<Revistas>() ;
	private ArrayList <Libros> libros = new ArrayList<Libros>();
	
	
	public void setRevistas(Revistas a, Revistas b, Revistas c, Revistas d) {
		 revistas.add(a);
         revistas.add(b);
         revistas.add(c);	
         revistas.add(d);
		
	}
	
	public void setLibros(Libros n, Libros hn) {
		
		libros.add(n);
        libros.add(hn);
    
	}
	

	public void buscar(String Nombre) {
		
		LocalDateTime fecha = LocalDateTime.now();
		
		for (Revistas r : this.revistas) {
			if (Nombre == r.GetTitulo()) {
				r.Prestar();
				r.imprimir_informacion();
				
				if (r.genero() =="comic") {
					
					int tiempo = r.Tiempo_fuera();
					if(tiempo == 24) {
						r.Perdido();
						System.out.println("Fecha actual: "+fecha.plusHours(tiempo) + " Fecha del prestamo:" + fecha);
					}
				}else if(r.genero() =="científico" || r.genero() == "deportiva") {
					
					int tiempo = r.Tiempo_fuera();
					if (tiempo >= 2) {
						r.Perdido();
						System.out.println("Fecha actual: "+fecha.plusDays(tiempo) + " Fecha del prestamo:" + fecha);
					}
				}else if (r.genero() == "gastronómia") {
					
					int tiempo = r.Tiempo_fuera();
					if (tiempo >= 3) {
						r.Perdido();
						System.out.println("Fecha actual: "+fecha.plusDays(tiempo) + " Fecha del prestamo:" + fecha);
					}
					
				}
				
			}
		}
		for (Libros r : this.libros) {
			if (Nombre == r.GetTitulo()) {
				
				r.Prestar();
				r.imprimir_informacion();
				if (r.Tiempo_fuera() > 5) {
					int pagar = (r.Tiempo_fuera() - 5)*1290;
					System.out.println("Usted debe :" + pagar +"$, ya que paso de la fecha limite");
					System.out.println("Fecha actual: "+fecha.plusDays(r.Tiempo_fuera()) + " Fecha del prestamo:" + fecha);
				}
			}	
		}
	
	}
}
